import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';

function App() {
  const[sum, setSum]=useState<number>()
  function submit(e:any){
    e.preventDefault()
    let url= "http://backend.app-felipe.129.159.126.70.sslip.io"
    switch (e.target.type.value) {
      case "sum":
        url=url+"/app/sum"
        break;
      case "multiply":
        url=url+"/app/multiplication"
        break;

      case "subtraction":
        url=url+"/app/subtract"
        break;
      case "division":
        url=url+"/app/division"
        break;
    
      default:
        break;
    }
    axios.post(url, {
      n1: e.target.value1.value,
      n2: e.target.value2.value
    })
    .then((result)=>{
      setSum(result.data)
    })
    .catch((error)=>{
      console.log(error)
    })
  }
  return (
    <div className="sum">
      <h2>Operations</h2>
      <form onSubmit={(e)=>submit(e)}>
        <input type="number" name='value1' />
        <input type="number" name='value2' />
        <select name="type">
          <option value="sum">Soma</option>
          <option value="subtraction">Subtração</option>
          <option value="division">Divisão</option>
          <option value="multiply">Multiplicação</option>
        </select>
        <button type='submit'> Calculate </button>
      </form>
      <h2>Result: {sum}</h2>
    </div>
  );
}

export default App;
